package main

import (
	"fmt"

	"felixvon.cn/amaterasu-go/lib/stringutils"
	"github.com/google/go-cmp/cmp"
)

func main() {
	fmt.Printf("hello, world\n")
	fmt.Println(stringutils.ReverseRunes("!oG ,olleH"))
	fmt.Println(cmp.Diff("Hello World", "Hello Go"))
}
